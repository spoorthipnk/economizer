package com.project.economizer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class MainScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, new HomeFragment())
                .commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content, new HomeFragment())
                            .commit();
                    return true;
                case R.id.navigation_dashboard:
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content, new DreamBoardFragment())
                            .commit();

                    return true;
                case R.id.navigation_notifications:

                    return true;
            }
            return false;
        }

    };

}

//Todo Enter the amount saved
//Todo How did you save? (Optional)
//Todo Create goals
//ToDo Deadlines and pay periods
//Todo Push notifications?
//ToDO Prioritize goals
//ToDo Change currency depending on country
//ToDo Create a cause/wish list/ Dream board
//ToDo Custom Keyboard
//Todo Show all the causes in a grid?
//Todo Save for a cause
//Todo UI - staggeredlayout with flip view?
//ToDo UI - A jar_money that keeps on filling up based on % ?
//ToDo $ Monitor - Enter salary and create an upper limit on the % of salary you want to spend on various categories - Home/Food/Groceries/Entertainment/Shopping/Personal etc
//ToDo Notify the user if he nears the threshold of his expenses
//ToDo Do something if he crosses the limit - decrease the amount for the next month or so
//ToDo Allow me to just hit the plus sign at the top to add money to the general overall fund for all goals, and the app automatically allocates the money to each goal in order of prioritization, and adds overages (any amount over completed/achieved goal amount) to the next goal.
//ToDO please add currencies like bitcoin.
//ToDO delete items  & could do with target dates (beginning and end) and a quick mathematical equation on average monthly savings as to if you're on target (or what you're on target for)
//ToDo a chart though that tells you how much you need to save monthly to reach your goal
//ToDo a feature to show how much would be needed to put per week/biweek to reach said goals
//ToDo To be able to select the date you added money to the savings account.
//ToDo Built in calculator

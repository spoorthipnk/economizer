package com.project.economizer;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class DreamBoardFragment extends Fragment {

    @BindView(R.id.buttonAddCause)
    ImageView btnAddCause;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dream_board, container, false);
        ButterKnife.bind(this,view);
        btnAddCause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content, new AddNewCauseFragment())
                        .commit();
            }
        });
        return view;
    }

    /*@OnClick(R.id.buttonAddCause)
    public void addCause(){

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, new AddNewCauseFragment())
                .commit();
    }*/

}
